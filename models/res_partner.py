# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class PartnerCode(models.Model):
    _inherit = 'res.partner'

    ref = fields.Char(
        readonly=True,
    )

    @api.model
    def update_with_partner_code(self):
        for partner in self.search([('ref', '=', False)]):
            partner.ref = self.env['ir.sequence'].next_by_code('res.partner.code') or _('New')

    @api.model
    def create(self, vals):
        vals['ref'] = self.env['ir.sequence'].next_by_code('res.partner.code') or _('New')
        result = super(PartnerCode, self).create(vals)
        return result
