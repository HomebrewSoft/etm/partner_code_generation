# -*- coding: utf-8 -*-
{
    'name': 'Partner Code Generation',
    'version': '13.0.0.1.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
    ],
    'data': [
        'data/ir_actions_server.xml',
        'data/ir_sequence.xml',
    ],
}
